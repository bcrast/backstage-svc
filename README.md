Backstage Coding Challenge

Setup
-----


1) Clone repo `git clone git@gitlab.com:bcrast/backstage-svc.git`

2) here you have 2 options
a) Install python 3 version of pipenv. for me this turned out to be `sudo -i pip3 install pipenv`.
b) continue on to the next step and just use the requirements.txt file, pip, and virtualenv

3) If using pipenv, initialize your project with `pipenv install`. This will
create your virtualenv and install all your packages for you. If you want to use 
the regular tools, create a virtualenv with `python3 -m venv ve`,
 activate your environment with `. ve/bin/activate`, and install 
requirements with `pip install -r requirements.txt`. If you choose 
the latter method you also need to `export FLASK_APP=svc/index.py` before you 
can start the app in the next step

4) if you choose pipenv you can either activate the pipenv shell with `pipenv shell`
or prefix all commands with `pipenv run` to load your environment variables
and run those commands in the virtualenv pipenv created for you 

5) create the db with `flask initdb` (`pipenv run flask initdb` if you want to run pipenv)

6) start the app with `flask run` (`pipenv run flask run`) and visit
`localhost:5000/differences/[number]` to see the service in action

7) visit `localhost:5000/triplets?a=[int]&b=[int]&c=[int]` to see the Pythagorean triplet
service

8) to run tests (or in this case, test), its `pytest` or `pipenv run pytest`

9) to check the type annotations with mypy run `mypy ./ --ignore-import-errors`.
the `--the --ignore-import-errors` flag prevents it from complaining about missing stub files.

 
Discussion
----------

For a small project like this I find pipenv to be super helpful for 
a few reasons. First, it automatically creates your virtualenv and runs things in 
that context. This is way more fun than having to activate your virtualenv by hand
Second, pipenv not only creates a Pipfile when you install packages, but also a
lock file (Pipfile.lock) that stores a bunch of stuff for build reproducibility. 
Third, pipenv automatically loads environment variables if you put them in a file
called `.env` and leave it in your project directory.  This is great for flask since
flask asks you to set FLASK_APP=[path to main file]. This is cool for a containerized
environment but pretty annoying if you have to keep setting FLASK_APP=svc/index.py
every time you leave and reenter the project directory. This is what you have to do
if you decide to just use the requirements.txt file I generated from the Pipfile with
`pipenv lock -r > requirements.txt`.  Pipenv is eventually going to be the standard 
unifying tool instead of having to manage requirements and virtual environments
separately. However, pipenv isn't quite ready for primetime on larger projects as
it is still relatively immature and buggy.  Two issues I have run into in my travels
are that in containerized environments, pipenv always creates a virtualenv, even
if you tell it to install packages with the system python with the `--system` flag,
and not all pip features work with pipenv, like installing a package from git with 
extra build flags just isn't possible with pipenv. TLDR, Pipenv is great for small 
projects like this but if you want to stick to pip + requirements.txt you can
do that as well.

I completed all the tasks besides the frontend react one. I went with sqlite for
my database because it was the quickest to set up, but sqlalchemy makes 
it easy to switch out dbs for a more robust solution if the need arose.
Hope you like it!

Ben Crastnopol




