import pytest
from flask import json
from flask_restful import Api

from svc.differences import Difference
from svc.errors import BadNumber
from svc.testing import EndpointTest


class TestDifferences(EndpointTest):

    def test_differences(self, client) -> None:

        resp = client.get('/difference/10')
        resp_data = json.loads(resp.get_data())
        assert resp_data['value'] == 2640
        assert resp_data['number'] == 10
        assert resp_data['occurrences'] == 1
        assert resp_data['last_datetime'] is None
        resp2 = client.get('difference/10')
        resp_data2 = json.loads(resp2.get_data())
        for v in ['value', 'number']:
            assert resp_data2[v] == resp_data[v]
        assert resp_data2['occurrences'] == resp_data['occurrences'] + 1
        assert resp_data2['last_datetime'] == resp_data['datetime']
        resp3 = client.get('difference/11')
        resp_data3 = json.loads(resp3.get_data())
        assert resp_data3['number'] == 11
        assert resp_data3['last_datetime'] == resp_data3['datetime']
        with pytest.raises(BadNumber):
            resp4 = client.get('/difference/100000')
        resp5 = client.get('/difference/bad')
        assert resp5.status_code == 404

