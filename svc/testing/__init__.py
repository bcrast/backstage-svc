import pytest
from flask_restful import Api

from svc.app import app
from svc.differences import Difference
from svc.models import teardown_db, get_engine, setup_db
from svc.triplets import Triplets

app.config['db_path'] = 'sqlite:///backstage-test.db'




def setup_module(client) -> None:
    # make sure db is alw
    teardown_db()


class EndpointTest:
    engine = get_engine()

    def setup_method(self) -> None:
        setup_db()

    def teardown_method(self) -> None:
        teardown_db()
