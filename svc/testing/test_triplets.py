import pytest
from flask import json

from svc.errors import BadNumber, WrongType
from svc.testing import EndpointTest


class TestTriplets(EndpointTest):

    def test_triplets(self, client) -> None:

        resp = client.get('/triplets?a=1&b=2&c=3')
        resp_data = json.loads(resp.get_data())
        assert resp_data['pythagorean_triplets'] is False
        assert resp_data['product'] == 6
        resp2 = client.get('/triplets?a=1&b=2&c=2')
        resp_data2 = json.loads(resp2.get_data())
        assert resp_data2['pythagorean_triplets'] is True
        assert resp_data2['product'] == 4
        with pytest.raises(BadNumber):
            resp3 = client.get('/triplets?a=1&b=2&c=10000')
        resp4 = client.get('/triplets')
        assert resp4.status_code == 400
        with pytest.raises(WrongType):
            resp5 = client.get('/triplets?a=1&b=2&c=a')
