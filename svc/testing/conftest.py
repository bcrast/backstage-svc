import pytest
from flask_restful import Api

from svc.app import app
from svc.differences import Difference
from svc.triplets import Triplets

test_client = app.test_client()
# have to register here, would normally find a way to centralize this logic
test_api = Api(test_client.application)
test_api.add_resource(Difference, '/difference/<int:number>')
test_api.add_resource(Triplets, '/triplets')


@pytest.fixture
def client(request):

    def teardown():
        pass

    request.addfinalizer(teardown)
    return test_client
