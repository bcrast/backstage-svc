import click
from typing import Dict, Any

from flask import Flask, jsonify
from flask_restful import Api

from .errors import BaseException
from .differences import Difference
from .models import setup_db
from .app import app
from .triplets import Triplets

api = Api(app)


@app.cli.command()
def initdb() -> None:
    click.echo('setting up db')
setup_db()


@app.route('/')
def hello() -> str:
    return 'Hello world'


@app.errorhandler(BaseException)
def handle_bad_number(error) -> Dict[str, Any]:
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


api.add_resource(Difference, '/difference/<int:number>')
api.add_resource(Triplets, '/triplets')

if __name__ == '__main__':
    app.run(debug=True)
