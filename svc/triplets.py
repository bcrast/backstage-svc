from flask_restful import Resource
from flask import request
from typing import Any, Dict
from .errors import BadNumber, WrongType


class Triplets(Resource):
    def get(self) -> Dict[str, Any]:
        """
         determines whether a sequence of three natural numbers (a,b and c) are a Pythagorean triplet
         and the product of the sequence of these three numbers where abc = n,
         where c is guaranteed to be no greater than 1000.
        :return: {'pythagorean_triplet': Bool, 'product': a * b *
        }
        """
        args = request.args
        try:
            a: int = int(args['a'])
            b: int = int(args['b'])
            c: int = int(args['c'])
        except ValueError:
            raise WrongType('arguments must be integers', status_code=400)

        if any(i > 1000 for i in [a, b, c]) or any(i < 0 for i in [a, b, c]):
            raise BadNumber("args have to be natural numbers no greater than 1000", status_code=400)
        p_triplet: int = (a ** 2) * (b ** 2) == c ** 2
        return {
            'pythagorean_triplets': p_triplet,
            'product': a * b * c
        }
