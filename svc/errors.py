from typing import Dict, Any


class BaseException(Exception):
    status_code = 400

    def __init__(self, message: str, status_code: int = None,
                 payload: Dict[str, Any] = None) -> None:
        super(BaseException, self).__init__()
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self) -> Dict[str, Any]:
        rv: Dict[str, Any] = dict(self.payload or {})
        rv['message'] = self.message
        return rv


class BadNumber(BaseException):
    pass


class WrongType(BaseException):
    pass
