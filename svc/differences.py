import datetime
from typing import Dict, Any, Tuple

from flask_restful import Resource
from sqlalchemy.orm import Session

from .models import get_session, Differences
from .errors import BadNumber


def _date_fmt(dt: datetime.datetime) -> str:
    """
    Datetime to string formatting helper function so dates are consistently formatted
    :param dt: datetime.datetime object
    :return: formatted date string
    """
    return dt.strftime('%x %X')


class Difference(Resource):

    def get(self, number: int) -> Dict[str, Any]:
        """
        Resource to calculate the difference
        between the sum of squares and the square of the sum of numbers between 1 and n
        where n <= 100

        :param number: number between 1 and 100
        :return: json blob like so: {
            "datetime":current_datetime,
            "value":solution,
            "number":n,
            "occurrences":occurrences, // the number of times n has been requested
            "last_datetime": datetime_of_last_request,
            }
        """
        if number > 100 or number < 1:
            raise BadNumber("Number isn't between 1 and 100", status_code=400)
        numbers: range = range(1, number + 1)
        dt: datetime.datetime = datetime.datetime.now()
        sess: Session = get_session()
        # get last timestamp before adding the new row
        last_timestamp: Tuple[datetime.datetime] = sess.query(Differences.timestamp).order_by(
            Differences.timestamp.desc()).first()
        sess.add(Differences(timestamp=dt, number=number))
        sess.commit()
        # get count after adding the new row so that row is included in the count
        requests_for_n: int = sess.query(Differences.number).filter(Differences.number == number).count()
        sum_of_squares: int = sum(i ** 2 for i in numbers)  # numpy could probably make these calculations go faster
        square_of_sum: int = sum(i for i in numbers) ** 2
        solution: int = square_of_sum - sum_of_squares
        return {
            'value': solution,
            'number': number,
            'occurrences': requests_for_n,
            'datetime': _date_fmt(dt),
            'last_datetime': _date_fmt(last_timestamp[0]) if last_timestamp else None
        }
