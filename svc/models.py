from sqlalchemy import Column, Integer, DateTime
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session

from .app import app

# declarative base is not a valid class for mypy so I have to ignore the error
Base = declarative_base()


class Differences(Base):  # type: ignore
    __tablename__ = 'differences'
    id: Column = Column(Integer, primary_key=True)
    timestamp: Column = Column(DateTime, nullable=False)
    number: Column = Column(Integer, nullable=False)


def get_engine():
    return create_engine(app.config['db_path'])


def setup_db() -> None:
    Base.metadata.create_all(get_engine())


def teardown_db() -> None:
    Base.metadata.drop_all(get_engine())


def get_session() -> Session:
    session: sessionmaker = sessionmaker(bind=get_engine())
    return session()
